import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.*;

public class Main {

    public static void main(String[] args) throws IOException {
	System.out.println("Version 3");
        Map<String, String> properties = getProperties();

        String sourceDirPath = properties.get("source_dir");
        String targetDirPath = properties.get("target_dir");
        String deltaMinText = properties.get("delta_minutes");

        l("sourceDir: " + sourceDirPath);
        l("targetDir: " + targetDirPath);
        l("deltaMin: " + deltaMinText);
        if (sourceDirPath == null || sourceDirPath.isEmpty() || targetDirPath == null
                || targetDirPath.isEmpty() || deltaMinText == null || deltaMinText.isEmpty()) {
            return;
        }

        File sourceDir = new File(sourceDirPath);
        if (!sourceDir.isDirectory()) {
            l("sourceDir is not a directory");
            return;
        }

        File targetDir = new File(targetDirPath);
        if (!targetDir.isDirectory()) {
            l("targetDir is not a directory");
            return;
        }

        int deltaMin;
        try {
            deltaMin = Integer.parseInt(deltaMinText);
        } catch (Exception e) {
            l("deltaMin is not a number. " + e.toString());
            return;
        }

        long timeStart = System.currentTimeMillis();

        // From witch time need to find last changed files
        Calendar fromCal = Calendar.getInstance();
        fromCal.add(Calendar.MINUTE, -deltaMin);
        l("start searching for files that have changed from: " + fromCal.getTime());
        FileTime fromFileTime = FileTime.fromMillis(fromCal.getTimeInMillis());

        // get last changed files
        List<File> lastChangedFiles = new ArrayList<>();
        getLastModifiedFiles(sourceDir, fromFileTime, lastChangedFiles);

        // Copy files to target
        l("start copying " + lastChangedFiles.size() + " files");
        lastChangedFiles.forEach(f -> {
            String path = f.getPath();
            String newPath = targetDirPath + "/" + path.substring(sourceDirPath.length());

            try {
                FileUtils.copyFile(f, new File(newPath));
            } catch (IOException e) {
                l("has a problem while copy a file from " + f.getPath() + ", to " + newPath + ". " + e.toString());
            }
        });
        long spentSeconds = (System.currentTimeMillis() - timeStart) / 1000L;
        long spentMinutes = spentSeconds / 60L + (spentSeconds % 60 != 0 ? 1L : 0L);
        l("finished by: " + spentSeconds + " seconds (or about " + spentMinutes + " minutes)");
        l(lastChangedFiles.size() + " files have been copied");
    }

    private static void getLastModifiedFiles(File dir, FileTime fromFileTime, List<File> files) throws IOException {
        File[] fList = dir.listFiles();
        if (fList == null) {
            return;
        }

        for (File file : fList) {
            if (file.isFile()) {
                Path path = Paths.get(file.getPath());
                BasicFileAttributes attr = Files.readAttributes(path, BasicFileAttributes.class);
                if (attr.lastModifiedTime().compareTo(fromFileTime) < 0) {
                    continue;
                }
                files.add(file);
                l(file.getPath());
            } else if (file.isDirectory()) {
                getLastModifiedFiles(file, fromFileTime, files);
            }
        }
    }


    private static Map<String, String> getProperties() {
        Properties property = new Properties();

        Map<String, String> map = new HashMap<>();
        try {
            FileInputStream fis = new FileInputStream("config.properties");
            property.load(fis);

            map.put("source_dir", property.getProperty("source_dir"));
            map.put("target_dir", property.getProperty("target_dir"));
            map.put("delta_minutes", property.getProperty("delta_minutes"));
        } catch (IOException e) {
            l("Can't find config.properties file");
            e.printStackTrace();
        }
        return map;
    }

    private static void l(String log) {
        System.out.println(log);
    }
}
