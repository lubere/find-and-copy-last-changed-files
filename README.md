Configured by a config.properties file, where<br>
<b>source_dir</b> - path where to look for files<br>
<b>target_dir</b> - path where to copy files<br>
<b>delta_minutes</b> - searches for files that have changed in the last number of minutes that specified here<br>
<br><br>
Example:<br>
source_dir = /Users/Pavel/Downloads<br>
target_dir = /Users/Pavel/_TRASH/target<br>
delta_minutes = 60<br>
